var database = [{
        username: "Pod",
        password: "secret"
    },
    {
        username: "Apollo",
        password: "secret"
    },
    {
        username: "Jacqueline",
        password: "secret"
    }
];

var newsFeed = [{
        username: "Apollo",
        timeline: "Walk? Food? Play?"
    },
    {
        username: "Jacqueline",
        timeline: "I love coding!!!"
    }
]

function isUserValid(username, password) {
    for (var i = 0; i < database.length; i++) {
        if (database[i].username === username &&
            database[i].password === password) {
            return true;
        }
    }
    return false;
}

function logIn(username, password) {
    if (isUserValid(username, password)) {
        console.log(newsFeed);
    } else {
        alert("Who the heck are you?!?!");
    }
}


var userNamePrompt = prompt("Username:");
var pwordPrompt = prompt("Password:");

logIn(userNamePrompt, pwordPrompt);
